//array  of products
var Products = []

function addEntry() 
{
    // Parse the JSON stored in allEntriesP
    var old_products = JSON.parse(localStorage.getItem("Products"));
    if (old_products == null) {
        old_products = [];
    }

    //getting elements from html form
    var inputProductName = document.getElementById("pname").value;
    var inputQuantity = document.getElementById("qty").value;
    var inputCountry = document.getElementById("country").value;
    var inputPrice = document.getElementById("price").value;

    console.log(inputProductName);
    console.log(inputQuantity);
    console.log(inputCountry);
    console.log(inputPrice);

    var products = 
    {
            "ProductName": inputProductName,
            "Quantity": inputQuantity,
            "Country": inputCountry,
            "Price": inputPrice,
    };

    Products = old_products;
    Products.push(products)
    localStorage.setItem("Products", JSON.stringify(Products));
};